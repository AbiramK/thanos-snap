# Thanos-Snap

[![PyPI version](https://badge.fury.io/py/thanos-snap.svg)](https://badge.fury.io/py/thanos-snap)

>A python command line tool that removes half of your files in the current directory.

## Install

```
$ pip install thanos-snap
```
## Usage

<img src="https://gitlab.com/AbiramK/thanos-snap/raw/master/usage.png" width="300">

## License

[MIT][License]

[LICENSE]: https://mit-license.org/